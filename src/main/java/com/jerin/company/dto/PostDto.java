package com.jerin.company.dto;

import lombok.*;

import java.io.Serializable;
import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class PostDto implements Serializable {
    private Long    ID;
    private UserDto user;
    private String  postHead;
    List<CommentDto> comments;
}
