package com.jerin.company.dto;

import lombok.*;

import java.io.Serializable;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class CommentDto implements Serializable {
    private Long ID;
    private String text;
    private UserDto user;
}
