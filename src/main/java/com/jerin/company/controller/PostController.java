package com.jerin.company.controller;

import com.jerin.company.Exception.ItemNotFoundException;
import com.jerin.company.bean.RestResponseFactory;
import com.jerin.company.dto.PostDto;
import com.jerin.company.service.PostService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/post")
public class PostController {

    private final PostService         postService;
    private final RestResponseFactory restResponseFactory;

    public PostController(final PostService postService, final RestResponseFactory restResponseFactory) {
        this.postService = postService;
        this.restResponseFactory = restResponseFactory;
    }

    @PostMapping(value = "/add-post")
    public ResponseEntity addPost() throws Exception{
        log.info("inside add post");
        PostDto post = postService.addPost();
        if(post == null ) {
            throw new Exception();
        }
        return ResponseEntity.ok(restResponseFactory.success("f7256238", "post-added", post));
    }

    @GetMapping(value = "/get-post")
    public ResponseEntity getComment(@RequestParam Long id) throws Exception{
        log.info("inside get post");
        PostDto postDto = postService.getPost(id);
        if(postDto == null ) {
            throw new ItemNotFoundException("fd1becbe", "post not found", new Exception());
        }
        return ResponseEntity.ok(restResponseFactory.success("fbb55240", "post-fetched", postDto));
    }

    @PutMapping(value = "/edit-post")
    public ResponseEntity editComment(@RequestBody PostDto postDto) throws Exception{
        log.info("inside edit post");
        PostDto postDtoResult = postService.editPost(postDto);
        if(postDtoResult == null ) {
            throw new Exception();
        }
        return ResponseEntity.ok(restResponseFactory.success("03faec44", "post-edited", postDtoResult));
    }

    @DeleteMapping(value = "/delete-post")
    public ResponseEntity deleteComment(@RequestParam Long id) throws Exception{
        log.info("inside delete post");
        if(id == null ) {
            throw new Exception();
        }
        postService.deletePost(id);
        return ResponseEntity.ok(restResponseFactory.success("0944f60e", "post-deleted", id));
    }

}
