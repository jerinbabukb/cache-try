package com.jerin.company.controller;

import com.jerin.company.Exception.ItemNotFoundException;
import com.jerin.company.bean.RestResponseFactory;
import com.jerin.company.dto.UserDto;
import com.jerin.company.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    private final UserService userService;
    private final RestResponseFactory restResponseFactory;

    public UserController(final UserService userService, final RestResponseFactory restResponseFactory) {
       this.userService = userService;
       this.restResponseFactory = restResponseFactory;
    }

    @GetMapping(value = "/get-user")
    public ResponseEntity getUser(@RequestParam Long id) throws Exception{
        log.info("inside get user");
        UserDto userDtoResult = userService.getUser(id);
        if(userDtoResult == null ) {
            throw new ItemNotFoundException("3621fefe", "user not found", new Exception());
        }
        return ResponseEntity.ok(restResponseFactory.success("12712342", "user-fetched", userDtoResult));
    }

    @PostMapping(value = "/add-user")
    public ResponseEntity addUser(@RequestBody UserDto user) throws Exception{
        log.info("inside add user");
        UserDto userDtoResult = userService.addUser(user);
        if(userDtoResult == null ) {
            throw new Exception();
        }
        return ResponseEntity.ok(restResponseFactory.success("16b035ce", "user-addded", userDtoResult));
    }

    @PutMapping(value = "/update-user")
    public ResponseEntity updateUser(@RequestBody UserDto userDto) throws Exception{
        log.info("update user");
        if(userDto == null ) {
            throw new Exception();
        }
        userService.editUser(userDto);
        return ResponseEntity.ok(restResponseFactory.success("1d9ca98a", "user-updated", userDto));
    }

    @DeleteMapping(value = "/delete-user")
    public ResponseEntity deleteUser(@RequestParam Long id) throws Exception{
        log.info("delete user");
        if(id == null ) {
            throw new Exception();
        }
        userService.deleteUser(id);
        return ResponseEntity.ok(restResponseFactory.success("2353cc0a", "user-updated", id));
    }
}