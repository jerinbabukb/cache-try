package com.jerin.company.controller;

import com.jerin.company.Exception.ItemNotFoundException;
import com.jerin.company.bean.RestResponseFactory;
import com.jerin.company.dto.CommentDto;
import com.jerin.company.service.CommentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/comment")
public class CommentController {

    private final CommentService      commentService;
    private final RestResponseFactory restResponseFactory;

    public CommentController(final CommentService commentService, final RestResponseFactory restResponseFactory) {
        this.commentService = commentService;
        this.restResponseFactory = restResponseFactory;
    }

    @PostMapping(value = "/add-comment")
    public ResponseEntity addComment() throws Exception{
        log.info("inside add comment");
        CommentDto commentDto = commentService.addComment();
        if(commentDto == null ) {
            throw new Exception();
        }
        return ResponseEntity.ok(restResponseFactory.success("d4faf8a8", "comment-added", commentDto));
    }
    @GetMapping(value = "/get-comment")
    public ResponseEntity getComment(@RequestParam Long id) throws Exception{
        log.info("inside get comment");
        CommentDto commentDto = commentService.getComment(id);
        if(commentDto == null ) {
            throw new ItemNotFoundException("2ea5d8ee", "comment not found", new Exception());
        }
        return ResponseEntity.ok(restResponseFactory.success("e1113f08", "comment-fetched", commentDto));
    }

    @PutMapping(value = "/edit-comment")
    public ResponseEntity editComment(@RequestBody CommentDto commentDto) throws Exception{
        log.info("inside edit comment");
        CommentDto commentDtoResult = commentService.editComment(commentDto);
        if(commentDtoResult == null ) {
            throw new Exception();
        }
        return ResponseEntity.ok(restResponseFactory.success("e6a6cd66", "comment-edited", commentDtoResult));
    }

    @DeleteMapping(value = "/delete-comment")
    public ResponseEntity deleteComment(@RequestParam Long id) throws Exception{
        log.info("inside delete comment");
        if(id == null ) {
            throw new Exception();
        }
        commentService.deleteComment(id);
        return ResponseEntity.ok(restResponseFactory.success("ecdda9a2", "comment-deleted", id));
    }
}
