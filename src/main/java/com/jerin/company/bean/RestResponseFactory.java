package com.jerin.company.bean;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

@Component
public class RestResponseFactory {
    private final MessageSource messageSource;

    public RestResponseFactory(final MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public RestResponse error(final String code, final String msgId) {
        return createMessage("ERROR", msgId, code, null);
    }

    public RestResponse success(final String code, final String msgId, final Object data) {
        return createMessage("SUCCESS", msgId, code, data);
    }


    private RestResponse createMessage(final String type, final String msgId, final String code, final Object data) {
        final RestResponse restResponse = new RestResponse();
        restResponse.setType(type);
        restResponse.setCode(code);
        restResponse.setData(data);
        restResponse.setMessageId(msgId);
        return restResponse;
    }
}
