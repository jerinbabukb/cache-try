package com.jerin.company.configuration;

import com.jerin.company.filter.AuthenticationFilter;
import com.jerin.company.filter.AuthorizationFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.logout.LogoutFilter;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    private final AuthenticationFilter authenticationFilter;
    private final AuthorizationFilter authorizationFilter;

    public SecurityConfiguration(final AuthorizationFilter authorizationFilter, final AuthenticationFilter authenticationFilter) {
        this.authenticationFilter = authenticationFilter;
        this.authorizationFilter = authorizationFilter;
    }

    @Bean
    public FilterRegistrationBean<AuthenticationFilter> authenticationFilterRegistartion(
            final AuthenticationFilter authenticationFilter) {
        final FilterRegistrationBean<AuthenticationFilter> authenticationFilterFilterRegistrationBean = new FilterRegistrationBean<>();
        authenticationFilterFilterRegistrationBean.setFilter(authenticationFilter);
        authenticationFilterFilterRegistrationBean.setEnabled(false);
        return authenticationFilterFilterRegistrationBean;
    }

    @Bean FilterRegistrationBean<AuthorizationFilter> authorizationFilterFilterRegistrationBean (
            final AuthorizationFilter authorizationFilter) {
        final FilterRegistrationBean<AuthorizationFilter> authorizationFilterFilterRegistrationBean = new FilterRegistrationBean<>();
        authorizationFilterFilterRegistrationBean.setFilter(authorizationFilter);
        authorizationFilterFilterRegistrationBean.setEnabled(false);
        return authorizationFilterFilterRegistrationBean;
    }

    @Override
    protected final void configure(final HttpSecurity http) throws Exception{
        http.httpBasic().disable();
        http.csrf().disable();

        http.sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.NEVER)
                .and()
                .addFilterBefore(authenticationFilter, LogoutFilter.class)
                .addFilterBefore(authorizationFilter, LogoutFilter.class);
    }

}
