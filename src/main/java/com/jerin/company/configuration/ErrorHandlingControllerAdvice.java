package com.jerin.company.configuration;

import com.jerin.company.Exception.ItemNotFoundException;
import com.jerin.company.bean.RestResponse;
import com.jerin.company.bean.RestResponseFactory;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@Slf4j
@ControllerAdvice
public class ErrorHandlingControllerAdvice {

    private final RestResponseFactory restResponseFactory;

    public ErrorHandlingControllerAdvice(final RestResponseFactory restResponseFactory) {
        this.restResponseFactory = restResponseFactory;
    }

    @ExceptionHandler(Exception.class)
    ResponseEntity<RestResponse> defaultExceptionHandler(
            final Exception ex, final HttpServletRequest request, final HttpServletResponse response) {

        if(response == null) {
            log.error("Error occured - " + ex.getMessage());
            return null;
        }

        response.setContentType("application/json");
        log.error("Error occured - " + ex.getMessage());
        return new ResponseEntity<>(restResponseFactory.error("a4447d0e", "error"), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(ItemNotFoundException.class)
    ResponseEntity<RestResponse> handleException(
            final ItemNotFoundException ex, final HttpServletRequest request, final HttpServletResponse response) {

        if(response == null) {
            log.error("Error occured - " + ex.getMessage());
          return null;
        }

        response.setContentType("application/json");
        log.error("Error occured - " + ex.getMessage());
        return new ResponseEntity<>(restResponseFactory.error("a905271c", ex.getMessage()), HttpStatus.NOT_FOUND);
    }
}
