package com.jerin.company;

import com.jerin.company.dto.CommentDto;
import com.jerin.company.dto.PostDto;
import com.jerin.company.dto.UserDto;
import com.jerin.company.entity.Comment;
import com.jerin.company.entity.Post;
import com.jerin.company.entity.User;
import java.util.ArrayList;
import java.util.List;

public class ConversionUtils {

    public static Comment convertToCommentFromCommentDto(CommentDto commentDto) {
        Comment comment = new Comment();
        comment.setID(commentDto.getID());
        comment.setText(commentDto.getText());
        comment.setUser(convertToUserFromUserDto(commentDto.getUser()));
        return comment;
    }

    public static User convertToUserFromUserDto(UserDto userDto) {
        User user = new User();
        user.setID(userDto.getID());
        user.setName(userDto.getName());
        return user;
    }

    public static CommentDto convertToCommentDtoFromComment(Comment comment) {
        CommentDto commentDto = new CommentDto();
        commentDto.setID(comment.getID());
        commentDto.setText(comment.getText());
        commentDto.setUser(convertToUserDtoFromUser(comment.getUser()));
        return commentDto;
    }

    public static PostDto convertToPostDtoFromPost(Post post) {
        PostDto postDto = new PostDto();
        postDto.setID(post.getID());
        postDto.setPostHead(post.getPostHead());
        postDto.setUser(convertToUserDtoFromUser(post.getUser()));
        postDto.setComments(getCommentsDtoListFromComments(post.getComments()));
        return postDto;
    }

    public static UserDto convertToUserDtoFromUser(User user) {
        UserDto userDto = new UserDto();
        userDto.setID(user.getID());
        userDto.setName(user.getName());
        return userDto;
    }

    public static List<CommentDto> getCommentsDtoListFromComments(List<Comment> comments) {
        List<CommentDto> listCommentDto = new ArrayList<CommentDto>();
        for(Comment comment : comments) {
            listCommentDto.add(convertToCommentDtoFromComment(comment));
        }
        return listCommentDto;
    }


    public static Post convertFromPostDto(PostDto postDto) {
        Post post = new Post();
        post.setPostHead(postDto.getPostHead());
        post.setID(postDto.getID());
        post.setUser(convertToUserFromUserDto(postDto.getUser()));
        post.setComments(getCommentsFromCommentsDtoList(postDto.getComments()));
        return post;
    }


    public static List<Comment> getCommentsFromCommentsDtoList(List<CommentDto> comments) {
        List<Comment> listComment = new ArrayList<Comment>();
        for(CommentDto commentDto : comments) {
            listComment.add(convertToCommentFromCommentDto(commentDto));
        }
        return listComment;
    }
}
