package com.jerin.company.Exception;

public class BaseException extends RuntimeException {
    private String code;

    public BaseException(final String code, final String message) {
        super(message);
        this.code = code;
    }

    public BaseException(final String code, final String message, final Exception e) {
        super(message, e);
        this.code = code;
    }

    private String getCode(){
        return this.code;
    }
}
