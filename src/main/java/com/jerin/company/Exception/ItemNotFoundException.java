package com.jerin.company.Exception;

public class ItemNotFoundException extends BaseException {

    public ItemNotFoundException(String code, String message, Exception e) {
        super(code, message, e);
    }
}
