package com.jerin.company;

import com.jerin.company.entity.Comment;
import com.jerin.company.entity.Post;
import com.jerin.company.entity.User;
import java.util.ArrayList;
import java.util.List;

public class Utils {
    public static Post createPost() {
        User user = createUser();
        Post post = new Post();
        List<Comment> comments = createComments(user);
        post.setPostHead("Njan Prakashan review");
        post.setUser(user);
        post.setComments(comments);
        return post;
    }

    public static User createUser() {
        User user = new User();
        user.setName("Jerin");
        return user;
    }

    public static Comment createComment(Post post) {
        Comment comment = new Comment();
        comment.setText("Good!");
        return comment;
    }

    public static List<Comment> createComments(User user) {

        List<Comment> comments = new ArrayList<Comment>();

        Comment comment1 = new Comment();
        comment1.setText("Good!");
        comment1.setUser(user);

        comments.add(comment1);

        Comment comment2 = new Comment();
        comment2.setText("Adipoli");
        comment2.setUser(user);

        comments.add(comment2);

        return comments;
    }
}
