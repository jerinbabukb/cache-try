package com.jerin.company.service;

import com.jerin.company.ConversionUtils;
import com.jerin.company.Utils;
import com.jerin.company.dto.PostDto;
import com.jerin.company.entity.Post;
import com.jerin.company.repository.PostRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@Transactional
public class
PostService {

    @Autowired
    PostRepository postRepository;

    public PostDto addPost() {
        Post post = Utils.createPost();
        PostDto postDto = ConversionUtils.convertToPostDtoFromPost(postRepository.save(post));
        return postDto;
    }

    public PostDto editPost(PostDto postDto) {

        PostDto postDtoResult = ConversionUtils.convertToPostDtoFromPost(
                postRepository.save(ConversionUtils.convertFromPostDto(postDto)));

        return postDtoResult;
    }

    @Cacheable(value = "postCache", key="#id")
    public PostDto getPost(Long id) throws Exception {
        Post post = postRepository.findById(id).orElse(null);
        if(post == null) {
            return null;
        }
        PostDto postDto = ConversionUtils.convertToPostDtoFromPost(post);
        return postDto;
    }

    public void deletePost(Long id) throws Exception {
        postRepository.deleteById(id);
    }
}
