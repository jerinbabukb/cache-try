package com.jerin.company.service;

import com.jerin.company.ConversionUtils;
import com.jerin.company.dto.UserDto;
import com.jerin.company.entity.User;
import com.jerin.company.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class UserService {
    @Autowired
    UserRepository userRepository;


    public UserDto addUser(UserDto userDto) {
        UserDto userDtoResult = ConversionUtils.convertToUserDtoFromUser(
                userRepository.save(ConversionUtils.convertToUserFromUserDto(userDto)));
        return userDtoResult;
    }

    public UserDto editUser(UserDto userDto) {
        UserDto userDtoResult = ConversionUtils.convertToUserDtoFromUser(
                userRepository.save(ConversionUtils.convertToUserFromUserDto(userDto)));
        return userDtoResult;
    }

    @Cacheable(value = "userCache", key="#id")
    public UserDto getUser(Long id) throws Exception {
        User user = userRepository.findById(id).orElse(null);
        if(user == null) {
            return null;
        }
        UserDto userDtoResult = ConversionUtils.convertToUserDtoFromUser(user);
        return userDtoResult;
    }

    public void deleteUser(Long id) {
        userRepository.deleteById(id);
    }
}
