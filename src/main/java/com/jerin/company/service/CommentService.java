package com.jerin.company.service;

import com.jerin.company.ConversionUtils;
import com.jerin.company.Utils;
import com.jerin.company.dto.CommentDto;
import com.jerin.company.entity.Comment;
import com.jerin.company.entity.Post;
import com.jerin.company.repository.CommentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
@Transactional
public class CommentService {

    @Autowired
    CommentRepository commentRepository;

    public CommentDto addComment() {
        Comment comment= Utils.createComment(new Post());
        CommentDto commentDto = ConversionUtils.convertToCommentDtoFromComment(commentRepository.save(comment));
        return commentDto;
    }

    public CommentDto editComment(CommentDto commentDto) {

        CommentDto commentDtoResult = ConversionUtils.convertToCommentDtoFromComment(
                commentRepository.save(ConversionUtils.convertToCommentFromCommentDto(commentDto)));

        return commentDtoResult;
    }

    @Cacheable(value = "commentCache", key="#id")
    public CommentDto getComment(Long id) throws Exception {
        Comment comment = commentRepository.findById(id).orElse(null);
        if(comment == null) {
            return null;
        }
        CommentDto commentDto = ConversionUtils.convertToCommentDtoFromComment(comment);
        return commentDto;
    }

    public void deleteComment(Long id) throws Exception {
        commentRepository.deleteById(id);
    }



}
