package com.jerin.company.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Table(name = "POST_TAB")
public class Post implements Serializable {

    private static Long serialVersionUid = 2L;

    @JoinColumn(name = "USER_ID")
    @OneToOne(cascade = CascadeType.ALL)
    User user;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "COMMENTS_ON_POST", joinColumns = {@JoinColumn(name = "POST_ID")},
            inverseJoinColumns = {@JoinColumn(name = "COMMENT_ID")})
    List<Comment> comments;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "POST_ID")
    private Long ID;
    @Column(name = "POST_HEADING")
    private String postHead;
}
