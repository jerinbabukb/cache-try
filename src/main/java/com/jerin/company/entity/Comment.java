package com.jerin.company.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Table(name = "COMMENT_TAB")
public class Comment implements Serializable {
    private static Long serialVersionUid = 3L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "COMMENT_ID")
    private Long ID;
    private String text;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "USR")
    private User user;
}
